﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody playerRB;
    private Animator playerAnimator;

    public ParticleSystem explosionParticle;
    public ParticleSystem dirtParticle;

    private AudioSource playerAudio;
    public AudioClip jumpSound;
    public AudioClip crashSound;

    public float jumpForce = 10;
    public float gravityModifier;
    public bool isOnGround = true;
    public bool gameOver = false;

    // Start is called before the first frame update
    void Start()
    {
        // Add RigidBody to playerRB
        playerRB = GetComponent<Rigidbody>();

        // Assign the controller animator to playerAnimator
        playerAnimator = GetComponent<Animator>();

        // Modify player's gravity
        Physics.gravity *= gravityModifier;

        // Assign the component AudioSource
        playerAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // Add force when Space key is pressed
        if (Input.GetKeyDown(KeyCode.Space) && isOnGround && !gameOver)
        {
            playerRB.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            isOnGround = false;

            //Set animation jump
            playerAnimator.SetTrigger("Jump_trig");
            
            //Stop particle when you jump
            dirtParticle.Stop();

            //Play sound when you jump
            playerAudio.PlayOneShot(jumpSound, 0.6f);
        }
    }


    private void OnCollisionEnter(Collision collision)
    {   // Check if the player is on collider with the ground
        if (collision.gameObject.CompareTag("Ground"))
        {
            isOnGround = true;
            dirtParticle.Play();

        // Check if the player is on collider with an obstacle
        }
        else if (collision.gameObject.CompareTag("Obstacle"))
        {
            // Set Game Over
            gameOver = true;
            Debug.Log("Game Over");

            // Set animation Death
            playerAnimator.SetBool("Death_b", true);
            playerAnimator.SetInteger("DeathType_int", 1);

            // Set Particles
            explosionParticle.Play();
            dirtParticle.Stop();

            //Play Sound when you die
            playerAudio.PlayOneShot(crashSound, 0.6f);
        }
    }
}
