﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public GameObject player;
    [SerializeField] Vector3 offset = new Vector3(0, 5, -7);

    // Update is called once per frame
    void LateUpdate()
    {
        //we transfert the postion of the player to the camera, then she follows him, we adding offset that is more practical than write new Vector3(0, 5, -7)
        transform.position = player.transform.position + offset;
    }
}
