﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //Private variables
    [SerializeField] readonly float speed = 10.0f;
    [SerializeField] const float turnSpeed = 25.0f;

    private float horizontalInput;
    private float verticalInput;


    // Update is called once per frame
    void FixedUpdate()
    {
        //We get Input Horizontal and Vertical axes
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");

        //We'll move the vehicle forward
        transform.Translate(Vector3.forward * Time.deltaTime * speed * verticalInput);
        //We turn the vehicle
        transform.Rotate(Vector3.up * Time.deltaTime * turnSpeed * horizontalInput);
    }
}
