﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    private float speed = 3.0f;
    private Rigidbody enemyRb;
    private GameObject playerGameObject;
    // Start is called before the first frame update
    void Start()
    {
        enemyRb = GetComponent<Rigidbody>();
        playerGameObject = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 directions = (playerGameObject.transform.position - this.transform.position).normalized;
        enemyRb.AddForce(directions * speed);

        if(this.transform.position.y < -10)
        {
            Destroy(this.gameObject);
        }
    }
}
