﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animation_light : MonoBehaviour
{
    [SerializeField]
    private float speed = 10f;

    private Vector2 lightMove = Vector2.zero;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector2.right * speed * Time.deltaTime);
    }
}
