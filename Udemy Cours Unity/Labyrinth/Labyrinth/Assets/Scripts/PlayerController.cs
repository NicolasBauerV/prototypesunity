﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
      [SerializeField]
        private float Speed = 4f, rot = 80f;

    [SerializeField]
    GameObject imGameOver;

    private float curspeed;

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            curspeed = Speed * 2;
        }
        else
        {
            curspeed = Speed;
        }

        transform.Rotate(Vector3.up * rot * Time.fixedDeltaTime * Input.GetAxis("Horizontal"));
        transform.Translate(Vector3.forward * curspeed * Time.deltaTime * Input.GetAxis("Vertical"));
    }

    public void GameOver()
    {
        imGameOver.SetActive(true);
        StartCoroutine(LoadMenu());
    }

    IEnumerator LoadMenu()
    {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("Menu");
    }
}
