﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    public bool CanOpen = false;

    [SerializeField]
    AudioClip soundOpen = null, soundDenied = null;

    [SerializeField]
    Animator openDoor;

    [SerializeField]
    GameObject endPoint;

    private void Awake()
    {
        myAudioSource = GetComponent<AudioSource>();
    }

    private AudioSource myAudioSource;
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && CanOpen == true)
        {
            openDoor.enabled = true;
            endPoint.SetActive(true);
            myAudioSource.PlayOneShot(soundOpen);
        }
        else
        {
            myAudioSource.PlayOneShot(soundDenied);
        }
    }
}
