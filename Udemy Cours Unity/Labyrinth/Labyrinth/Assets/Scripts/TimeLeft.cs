﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeLeft : MonoBehaviour
{
    [SerializeField]
    private int timeCountDown = 60;

    [SerializeField]
    Text txtCountDown;

    // Start is called before the first frame update
    void Start()
    {
        txtCountDown.text = "Time left : " + timeCountDown;
        StartCoroutine(Pause());
    }

    IEnumerator Pause()
    {
        while(timeCountDown > 0)
        {
            yield return new WaitForSeconds(1f);
            timeCountDown--;
            txtCountDown.text = "Time left : " + timeCountDown;
        }
        GameObject.Find("Player").GetComponent<PlayerController>().GameOver();
    }

}
