﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// This script demonstrate how to use the particle system collision callback.
/// The sample using it is the "Extinguish" prefab. It use a second, non displayed
/// particle system to lighten the load of collision detection.
/// </summary>
public class ParticleCollision : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Player")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
        }
    }
}
